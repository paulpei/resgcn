from __future__ import division
from __future__ import print_function
import os
# Train on CPU (hide GPU) due to memory constraints
os.environ['CUDA_VISIBLE_DEVICES'] = ""

import tensorflow as tf
#import tensorflow.compat.v1 as tf
from constructor import get_placeholder, get_model, get_optimizer, update
import numpy as np
from input_data import format_data
from sklearn.metrics import roc_auc_score, average_precision_score
import pandas as pd
from preprocessing import construct_feed_dict
# Settings
flags = tf.app.flags
FLAGS = flags.FLAGS
import numpy as np
import scipy.sparse as sp
from sklearn import linear_model
from rankmetrics.metrics import recall_k_score

def split_train_test(Y,p):
    n=Y.shape[0]
    n_p=int(n*p)
    indexes=np.arange(n)
    np.random.shuffle(indexes)
    indexes_train=indexes[:n_p]
    indexes_test=indexes[n_p:]
    
    return indexes_train,indexes_test


def recall_score(y_true, y_score, k=100):
    act_set = set(y_true)
    pred_set = set(y_score[:k])
    result = len(act_set & pred_set) / float(len(act_set))
    return result


def ranking_precision_score(y_true, y_score, k=100):
    """Precision at rank k
    Parameters
    ----------
    y_true : array-like, shape = [n_samples]
        Ground truth (true relevance labels).
    y_score : array-like, shape = [n_samples]
        Predicted scores.
    k : int
        Rank.
    Returns
    -------
    precision @k : float
    """
    unique_y = np.unique(y_true)

    if len(unique_y) > 2:
        raise ValueError("Only supported for two relevance levels.")

    pos_label = unique_y[1]
    n_pos = np.sum(y_true == pos_label)

    order = np.argsort(y_score)[::-1]
    y_true = np.take(y_true, order[:k])
    n_relevant = np.sum(y_true == pos_label)

    # Divide by min(n_pos, k) such that the best achievable score is always 1.0.
    return float(n_relevant) / min(n_pos, k)


def average_precision_score(y_true, y_score, k=100):
    """Average precision at rank k
    Parameters
    ----------
    y_true : array-like, shape = [n_samples]
        Ground truth (true relevance labels).
    y_score : array-like, shape = [n_samples]
        Predicted scores.
    k : int
        Rank.
    Returns
    -------
    average precision @k : float
    """
    unique_y = np.unique(y_true)

    if len(unique_y) > 2:
        raise ValueError("Only supported for two relevance levels.")

    pos_label = unique_y[1]
    n_pos = np.sum(y_true == pos_label)

    order = np.argsort(y_score)[::-1][:min(n_pos, k)]
    y_true = np.asarray(y_true)[order]

    score = 0.0
    for i in range(len(y_true)):
        if y_true[i] == pos_label:
            # Compute precision up to document i
            # i.e, percentage of relevant documents up to document i.
            prec = 0
            for j in range(0, i + 1):
                if y_true[j] == pos_label:
                    prec += 1.0
            prec /= (i + 1.0)
            score += prec

    if n_pos == 0:
        return 0

    return score / n_pos


class AnomalyDetectionRunner():
    def __init__(self, settings):
        self.data_name = settings['data_name']
        self.iteration = settings['iterations']
        self.model = settings['model']


    def erun(self,i):
        model_str = self.model
        # load data
        feas,labels_c = format_data(self.data_name)
        #save 
        np.save("save_data/"+str(i)+"_input_cora.npy",feas)
        
        print("feature number: {}".format(feas['num_features']))
        labels_c=labels_c.squeeze()
        print("classes",np.max(labels_c),labels_c.shape)
        labels_c_onehot=np.eye(7)[labels_c]
        
        indexes_train,indexes_test=split_train_test(labels_c,0.5)
        #########test##############on original data#########
        data_f=feas['features']
        sp_data=sp_data=sp.coo_matrix((data_f[1],(data_f[0][:,0],data_f[0][:,1])),shape=data_f[2])
        data_f_dense=sp_data.todense()
        X_train=data_f_dense[indexes_train]
        X_test=data_f_dense[indexes_test]
        Y_train=labels_c[indexes_train]
        Y_test=labels_c[indexes_test]
        regr=linear_model.SGDClassifier(max_iter=5000,tol=1e-3)
        regr.fit(X_train,Y_train)
        print("original data",regr.score(X_test,Y_test))
        ###########################################
        
        adj=feas['adj']
        pos_weight = float(adj.shape[0] * adj.shape[0] - adj.sum()) / adj.sum()
        norm = adj.shape[0] * adj.shape[0] / float((adj.shape[0] * adj.shape[0] - adj.sum()) * 2)
        
        # Define placeholders
        placeholders = get_placeholder()

        # construct model
        gcn_model = get_model(model_str, placeholders, feas['num_features'], feas['num_nodes'], feas['features_nonzero'])

        # Optimizer
        opt = get_optimizer(model_str, gcn_model, placeholders, feas['num_nodes'], FLAGS.alpha,norm,pos_weight,labels_c_onehot,indexes_train,indexes_test)

        # Initialize session
        sess = tf.Session()
        sess.run(tf.global_variables_initializer())

        # Train model
        best_auc=0
        for epoch in range(1, self.iteration+1):

            reconstruction_errors, reconstruction_loss,prediction = update(gcn_model, opt, sess, feas['adj_norm'], feas['adj_label'], feas['features'], placeholders, feas['adj'])
            if epoch % 10 == 0:
                print("Epoch:", '%04d' % (epoch), "train_loss=", "{:.5f}".format(reconstruction_loss),flush=True)
                #acc_train=np.sum(prediction[indexes_train]==labels_c[indexes_train])/indexes_train.shape[0]
                #acc_test=np.sum(prediction[indexes_test]==labels_c[indexes_test])/indexes_test.shape[0]
                #print("Epoch:", '%04d' % (epoch),"train_acc=","{:.5f}".format(acc_train),"test_acc=","{:.5f}".format(acc_test))
                
                
            if epoch % 100 == 0:
                y_true = [label[0] for label in feas['labels']]
                feed_dict = construct_feed_dict(feas['adj_norm'], feas['adj_label'], feas['features'], placeholders)
                feed_dict.update({placeholders['dropout']: FLAGS.dropout})
                R0=gcn_model.R0
                #R1=gcn_model.R11
                #R2=gcn_model.R22
                #Me_errors1=tf.sqrt(tf.reduce_sum(tf.square(R1),1)).eval(session=sess,feed_dict=feed_dict)
                #Me_errors2=tf.sqrt(tf.reduce_sum(tf.square(R2),1)).eval(session=sess,feed_dict=feed_dict)
                Me_errors3=tf.sqrt(tf.reduce_sum(tf.square(R0),1)).eval(session=sess,feed_dict=feed_dict)
                all_errors=Me_errors3
        
                auc_me=roc_auc_score(y_true,all_errors)
                
                auc = roc_auc_score(y_true, reconstruction_errors)
                if auc_me>best_auc:
                    best_auc=auc_me
                    feed_dict = construct_feed_dict(feas['adj_norm'], feas['adj_label'], feas['features'], placeholders)
                    feed_dict.update({placeholders['dropout']: FLAGS.dropout})
                    embedding=gcn_model.embeddings.eval(session=sess,feed_dict=feed_dict)
                    np.save("save_data/"+str(i)+"_embeddings_cora.npy",embedding)
                print("auc_me",auc_me,flush=True)
                print("auc",auc)
                print("Precision@100", ranking_precision_score(y_true, reconstruction_errors))
                print("Precision@100", ranking_precision_score(y_true, all_errors))
                print("Recall@100", recall_k_score([y_true], [reconstruction_errors], k=100))
                print("Recall@100", recall_k_score([y_true], [all_errors], k=100))
                
        #save embedding
        #feed_dict = construct_feed_dict(feas['adj_norm'], feas['adj_label'], feas['features'], placeholders)
        #feed_dict.update({placeholders['dropout']: FLAGS.dropout})
        #embedding=gcn_model.embeddings.eval(session=sess,feed_dict=feed_dict)
        #np.save("save_data/"+str(i)+"_embeddings_pubmed.npy",feas)
        
        
        sorted_errors = np.argsort(-reconstruction_errors, axis=0)
        with open('output/{}-ranking.txt'.format(self.data_name), 'w') as f:
            for index in sorted_errors:
                f.write("%s\n" % feas['labels'][index][0])
        df = pd.DataFrame({'AD-GCA':reconstruction_errors})
        df.to_csv('output/{}-scores.csv'.format(self.data_name), index=False, sep=',')





