import scipy.sparse as sp
import scipy.io
import inspect
import tensorflow as tf
#import tensorflow.compat.v1 as tf
from preprocessing import preprocess_graph, sparse_to_tuple
import numpy as np
flags = tf.app.flags
FLAGS = flags.FLAGS
import sys
import scipy.sparse as sp
import scipy.io
import numpy as np
from sklearn.decomposition import PCA

from random import shuffle
#import sys  

#reload(sys)  
#sys.setdefaultencoding('utf8')

#import cPickle
import pickle as pkl
import numpy as np
import scipy.sparse as sp
import pandas as pd

def load_data_embedding(data_source):
    objects = []
    names = ['x', 'y', 'tx', 'ty', 'allx', 'ally', 'graph']
    for i in range(len(names)):
        with open("data/ind.{}.{}".format(data_source, names[i]), 'rb') as f:
            if sys.version_info > (3, 0):
                objects.append(pkl.load(f, encoding='latin1'))
            else:
                objects.append(pkl.load(f))

    x, y, tx, ty, allx, ally, graph = tuple(objects)

    num = len(graph)
    print(num)
    A = [[0 for i in range(num)] for j in range(num)]
    for k, v in graph.items():
        for item in v:
            A[k][item] = 1

    X = np.concatenate((allx.todense(), tx.todense()))
    print(X.shape)
    #labels = np.argmax(np.concatenate((ally, ty)), 1)

    
    
    PCA_dim =20
    m = 15
    n = 20
    k = 50
    #pubmed 19717
    #cora 2708
    num_of_nodes = 2708

    X = PCA(n_components=PCA_dim).fit_transform(X)
    #X=np.array(data["Attributes"].todense())
    X=np.array(X)
    
    #print(X)
    max_=np.amax(X,axis=0,keepdims=True)
    min_=np.amin(X,axis=0,keepdims=True)
    X=(X-min_)/(max_-min_+1e-10)
    
    anomalies = add_attribute_anomalies(m, n, k, num_of_nodes, X)
    #anomalies=list(set(anomalies))
    labels = [[0] for i in range(num_of_nodes)]
    for i in anomalies:
        labels[i] = [1]
        
    
    
    
    labels_c = np.argmax(np.concatenate((ally, ty)), 1)

    return sp.lil_matrix(A), sp.lil_matrix(X), labels,labels_c


def largest_distance(target, neighbors, X):
    distance = []
    for item in neighbors:
        distance.append(np.linalg.norm(X[target] - X[item], ord=2, keepdims=True))
    largest_distance_index = np.argmax(distance)
    return neighbors[largest_distance_index]


def gen_cliques(m, indices):
    edges = []
    for i in range(m-1):
        for j in range(i+1, m):
            edges.append((indices[i], indices[j]))
    return edges


def add_structure_anomalies(m, n, num_of_nodes, A):
    indices = [i for i in range(num_of_nodes)]
    shuffle(indices)

    edges = []
    for i in range(n):
        edges += gen_cliques(m, indices[m*i: m*(i+1)])
    print(A.shape)
    for item in edges:
        (x, y) = item
        A[x,y] = 1
        A[y,x] = 1

    return indices[: m*n]


def add_attribute_anomalies(m, n, k, num_of_nodes, X):
    indices = [i for i in range(num_of_nodes)]
    shuffle(indices)

    anomalies = indices[: m*n]
    for a in anomalies:
        # select k random elements
        shuffle(indices)
        neighbors = indices[: k]
        select_id = largest_distance(a, neighbors, X)
        X[a] = X[select_id]

    return anomalies


def load_data_p(data_source):
    #data = scipy.io.loadmat(data_source)
    data = scipy.io.loadmat("data/{}.mat".format(data_source))
    labels = data["Network"]
    labels_c=data["Label"]
    PCA_dim = 32
    m = 30
    n = 10
    k = 300
    #5196 for b
    #7575 for f
    num_of_nodes = 5196

    X = PCA(n_components=PCA_dim).fit_transform(data["Attributes"].todense())
    #X=np.array(data["Attributes"].todense())
    X=np.array(X)
    
    #print(X)
    max_=np.amax(X,axis=0,keepdims=True)
    min_=np.amin(X,axis=0,keepdims=True)
    X=(X-min_)/(max_-min_+1e-10)
    
    
    A = data["Network"].todense()
    #print(A.shape)
    anomalies = add_structure_anomalies(m, n, num_of_nodes, A)
    anomalies += add_attribute_anomalies(m, n, k, num_of_nodes, X)
    #anomalies=list(set(anomalies))
    labels = [[0] for i in range(num_of_nodes)]
    for i in anomalies:
        labels[i] = [1]

    return sp.lil_matrix(A), sp.lil_matrix(X), labels,labels_c






def parse_index_file(filename):
    index = []
    for line in open(filename):
        index.append(int(line.strip()))
    return index

def load_data(data_source):
    data = scipy.io.loadmat("data/{}.mat".format(data_source))
    labels = data["gnd"]
    X=data["X"]
    max_=np.max(X,axis=0,keepdims=True)
    min_=np.min(X,axis=0,keepdims=True)
    X=(X-min_)/(max_-min_+1e-10)
    
    attributes = sp.csr_matrix(X)
    network = sp.lil_matrix(data["A"])
 
    return network, attributes, labels


def load_one_data(data_source):
    filepath = 'data/' + data_source + '/'
    structfile = 'struct.csv'
    contfile='content.csv'
    fileLabels = 'label.csv'
        
    A = pd.read_csv(filepath+structfile, header=None)
    A = A.as_matrix(columns=None)
    network = sp.lil_matrix(A)
    
    C = pd.read_csv(filepath+contfile, header=None)
    C = C.as_matrix(columns=None)

    X = np.array(PCA(n_components=20).fit_transform(C))
    attributes = sp.csr_matrix(X)
    
    #labels_c = pd.read_csv(filepath+fileLabels, header=None)
    #labels_c = labels_c[0].tolist()

    labels_c = []
    fin = open(filepath+fileLabels, 'r')
    for line in fin.readlines():
    	labels_c.append([int(line.strip())])
    fin.close()


    labels = []
    fin = open(filepath+'permutation.csv', 'r')
    for line in fin.readlines():
        if int(line.strip())>2707:
        	labels.append([1])
        else:
        	labels.append([0])
    fin.close()

    return network, attributes, labels, np.asarray(labels_c)


def format_data(data_source):

    #adj, features, labels,labels_c = load_one_data(data_source)
    adj, features, labels,labels_c = load_data_p(data_source) 
    #adj, features, labels,labels_c = load_data_embedding(data_source)

    # Store original adjacency matrix (without diagonal entries) for later
    # adj_orig = adj
    # adj_orig = adj_orig - sp.dia_matrix((adj_orig.diagonal()[np.newaxis, :], [0]), shape=adj_orig.shape)
    # adj_orig.eliminate_zeros()
    # adj = adj_orig

    if FLAGS.features == 0:
        features = sp.identity(features.shape[0])  # featureless

    # Some preprocessing
    adj_norm = preprocess_graph(adj)

    num_nodes = adj.shape[0]

    features = sparse_to_tuple(features.tocoo())
    num_features = features[2][1]
    features_nonzero = features[1].shape[0]

    adj_label = adj + sp.eye(adj.shape[0])
    adj_label = sparse_to_tuple(adj_label)
    items = [adj, num_features, num_nodes, features_nonzero, adj_norm, adj_label, features, labels]
    feas = {}
    for item in items:
        # item_name = [ k for k,v in locals().iteritems() if v == item][0]]
        item_name = retrieve_name(item)
        feas[item_name] = item

    return feas,labels_c

def retrieve_name(var):
    callers_local_vars = inspect.currentframe().f_back.f_locals.items()
    return [var_name for var_name, var_val in callers_local_vars if var_val is var and "item" not in var_name][0]
