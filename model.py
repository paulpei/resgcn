from layers import GraphConvolution, GraphConvolutionSparse, InnerProductDecoder, FullyConnectedDecoder
import tensorflow as tf
#import tensorflow.compat.v1 as tf
import tensorflow
flags = tf.app.flags
FLAGS = flags.FLAGS


class Model(object):
    def __init__(self, **kwargs):
        allowed_kwargs = {'name', 'logging'}
        for kwarg in kwargs.keys():
            assert kwarg in allowed_kwargs, 'Invalid keyword argument: ' + kwarg

        for kwarg in kwargs.keys():
            assert kwarg in allowed_kwargs, 'Invalid keyword argument: ' + kwarg
        name = kwargs.get('name')
        if not name:
            name = self.__class__.__name__.lower()
        self.name = name

        logging = kwargs.get('logging', False)
        self.logging = logging

        self.vars = {}

    def _build(self):
        raise NotImplementedError

    def build(self):
        """ Wrapper for _build() """
        with tf.variable_scope(self.name):
            self._build()
        variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name)
        self.vars = {var.name: var for var in variables}

    def fit(self):
        pass

    def predict(self):
        pass


class GCNModelAE(Model):
    def __init__(self, placeholders, num_features, features_nonzero, **kwargs):
        super(GCNModelAE, self).__init__(**kwargs)

        self.inputs = placeholders['features']
        self.input_dim = num_features
        self.features_nonzero = features_nonzero
        self.adj = placeholders['adj']
        self.adj_ori=tf.sparse.to_dense(placeholders["adj_orig"])
        self.dropout = placeholders['dropout']
        self.build()

    def _build(self):
        
        lam=0.05
        self.inputs1=tf.sparse.to_dense(self.inputs)        
                               
        self.inputs2=self.inputs1
        
        self.R00=FullyConnectedDecoder(input_dim=self.input_dim,output_dim=int(self.input_dim*2),act=tf.nn.relu,adj=self.adj)(self.inputs2)
        self.R01=FullyConnectedDecoder(input_dim=int(self.input_dim*2),output_dim=int(self.input_dim*2),act=tf.nn.relu,adj=self.adj)(self.R00)
        #self.R02=FullyConnectedDecoder(input_dim=int(self.input_dim*2),output_dim=int(self.input_dim*2),act=tf.nn.relu,adj=self.adj)(self.R01)
        
        self.R0=FullyConnectedDecoder(input_dim=int(self.input_dim*2),output_dim=self.input_dim,act=tf.nn.tanh,adj=self.adj)(self.R01)
        #self.R0=tf.Print(self.R0,["adj_R0",self.R0])
        #self.W0=tf.math.exp(-1*lam*tf.abs(self.R0))
        #self.R0=tf.nn.tanh(self.R00)
        
        self.W0=self.R0
        #self.W0=tf.Print(self.W0,["adj_ori",elf.W0])
        ###self.inputs2=self.inputs2-self.R0*0.05
        #self.inputs2=tf.clip_by_value(self.inputs2,0,1)
        #self.W0=(self.inputs1-self.inputs2)/0.05
        
        
        self.inputs3=tensorflow.contrib.layers.dense_to_sparse(self.inputs2)
        
        
        self.hidden1 = GraphConvolutionSparse(input_dim=self.input_dim,
                                              output_dim=FLAGS.hidden1,
                                              adj=self.adj,
                                              features_nonzero=self.features_nonzero,
                                              act=tf.nn.relu,
                                              dropout=self.dropout,
                                              logging=self.logging)(self.inputs3)
        
        self.R1=FullyConnectedDecoder(input_dim=self.input_dim,output_dim=int(FLAGS.hidden1),adj=self.adj)(self.R0)
        self.R11=FullyConnectedDecoder(input_dim=int(FLAGS.hidden1),output_dim=FLAGS.hidden1,act=tf.nn.relu,adj=self.adj)(self.R1)
        self.W1=tf.math.exp(-1*lam*tf.abs(self.R11))
        self.hidden1=self.hidden1*self.W1
        

        self.embeddings = GraphConvolution(input_dim=FLAGS.hidden1,
                                           output_dim=FLAGS.hidden2,
                                           adj=self.adj,
                                           act=lambda x:x,
                                           dropout=self.dropout,
                                           logging=self.logging)(self.hidden1)
        
        self.R2=FullyConnectedDecoder(input_dim=FLAGS.hidden1,output_dim=int(FLAGS.hidden2*2),adj=self.adj)(self.R11)
        self.R22=FullyConnectedDecoder(input_dim=int(FLAGS.hidden2*2),output_dim=FLAGS.hidden2,act=tf.nn.relu,adj=self.adj)(self.R2)
        self.W2=tf.math.exp(-1*lam*tf.abs(self.R22))
        self.embeddings=self.embeddings*self.W2
        
        
        #self.new_embeddings=tf.concat([self.embeddings,self.R22],axis=1)
        
        # self.z_mean = self.embeddings

        # decoder1

        #self.attribute_decoder_layer1 =FullyConnectedDecoder(input_dim=FLAGS.hidden2,output_dim=FLAGS.hidden1,adj=self.adj)(self.embeddings) 
        self.attribute_decoder_layer2 =FullyConnectedDecoder(input_dim=FLAGS.hidden2,output_dim=self.input_dim,adj=self.adj)(self.embeddings) 

        # decoder2
        self.structure_decoder_layer1 = FullyConnectedDecoder(input_dim=FLAGS.hidden2,output_dim=FLAGS.hidden1,adj=self.adj)(self.embeddings)

        self.structure_decoder_layer2 = InnerProductDecoder(input_dim=FLAGS.hidden1,
                                        act=tf.nn.relu,
                                        logging=self.logging)(self.structure_decoder_layer1)


        self.attribute_reconstructions = self.attribute_decoder_layer2
        self.structure_reconstructions = self.structure_decoder_layer2



class GCNModelVAE(Model):
    def __init__(self, placeholders, num_features, num_nodes, features_nonzero, **kwargs):
        super(GCNModelVAE, self).__init__(**kwargs)

        self.inputs = placeholders['features']
        self.input_dim = num_features
        self.features_nonzero = features_nonzero
        self.n_samples = num_nodes
        self.adj = placeholders['adj']
        self.dropout = placeholders['dropout']
        self.build()

    def _build(self):
        self.hidden1 = GraphConvolutionSparse(input_dim=self.input_dim,
                                              output_dim=FLAGS.hidden1,
                                              adj=self.adj,
                                              features_nonzero=self.features_nonzero,
                                              act=tf.nn.relu,
                                              dropout=self.dropout,
                                              logging=self.logging)(self.inputs)

        self.z_mean = GraphConvolution(input_dim=FLAGS.hidden1,
                                       output_dim=FLAGS.hidden2,
                                       adj=self.adj,
                                       act=lambda x: x,
                                       dropout=self.dropout,
                                       logging=self.logging)(self.hidden1)

        self.z_log_std = GraphConvolution(input_dim=FLAGS.hidden1,
                                          output_dim=FLAGS.hidden2,
                                          adj=self.adj,
                                          act=lambda x: x,
                                          dropout=self.dropout,
                                          logging=self.logging)(self.hidden1)

        self.z = self.z_mean + tf.random_normal([self.n_samples, FLAGS.hidden2]) * tf.exp(self.z_log_std)

        self.reconstructions = InnerProductDecoder(input_dim=FLAGS.hidden2,
                                      act=lambda x: x,
                                      logging=self.logging)(self.z)
